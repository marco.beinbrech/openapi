FROM node:14-buster-slim

WORKDIR /docs
ADD package* /docs/
RUN npm install
ADD . /docs/

